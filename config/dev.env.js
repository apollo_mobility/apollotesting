'use strict'

const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  remoteDBUrl: '"http://localhost:5984/"',
  sendEmailUrl: '"http://localhost:3000/send/email"',
  sendSMSUrl: '"http://localhost:3000/send/sms"'
})

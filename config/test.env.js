'use strict'

const merge = require('webpack-merge')
const devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  remoteDBUrl: '"http://127.0.0.1:5984/"',
  sendEmailUrl: '"http://127.0.0.1:3000/send/email"',
  sendSMSUrl: '"http://127.0.0.1:3000/send/sms"'
})

// module.exports = merge(devEnv, {
//   NODE_ENV: '"testing"',
//   remoteDBUrl: '"http://localhost:5984/"',
//   sendEmailUrl: '"http://localhost:3000/send/email"',
//   sendSMSUrl: '"http://localhost:3000/send/sms"'
// })

//localhost:5984
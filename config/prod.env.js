module.exports = {
  NODE_ENV: '"production"',
  remoteDBUrl: '"https://42c7a0b4-da0d-456a-a1f6-50b8f6482aa7-bluemix.cloudant.com/"',
  sendEmailUrl: '"https://d2ubb96ycc1bpp.cloudfront.net/send/email"',
  sendSMSUrl: '"https://d2ubb96ycc1bpp.cloudfront.net/send/sms"'
}

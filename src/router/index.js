import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/login/login.vue'
import Register from '@/components/register/register'
import Dashboard from '@/components/dashboard/dashboard'
import Wizard from '@/components/wizard/index'
import ViewJobs from '@/components/viewjobs/view-jobs'
import ViewJobDetail from '@/components/viewjobs/view-job-detail'

import Auth from '@/utils/auth'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard,
      meta: { requiresAuth: true }
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/new-job',
      name: 'Wizard',
      component: Wizard,
      meta: { requiresAuth: true }
    },
    {
      path: '/view-jobs',
      name: 'ViewJobs',
      component: ViewJobs,
      meta: { requiresAuth: true }
    },
    {
      path: '/job/:id/edit',
      name: 'EditWizard',
      component: Wizard,
      meta: { requiresAuth: true }
    },
    {
      path: '/job/:id',
      name: 'ViewJobDetail',
      component: ViewJobDetail,
      meta: { requiresAuth: true }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    let isAuth = Auth.loggedIn()
    if (!isAuth) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router

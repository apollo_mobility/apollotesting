import Vue from 'vue'
import VueCookie from 'vue-cookie'

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import ElementLocale from 'element-ui/lib/locale/lang/en'

import router from './router'
import App from './App'
import i18n from './lang/lang'

import './style/main.css'
import '@/assets/css/style.css'

Vue.config.productionTip = false
Vue.use(Element, { locale: ElementLocale })
Vue.use(VueCookie)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  router,
  template: '<App/>',
  components: {
    App: App
  },
  data: {
    pagetitle: 'Job Sheet'
  }
  /*,
  methods: {
    saveLocalDoc: function(doc) {
      return db
        .get(doc._id)
        .then(data => {
          doc._rev = data._rev;
          return db.put(doc);
        })
        .catch(e => {
          return db.put(doc);
        });
    },
    startSyncData: function() {
      this.sync = jobCardDb.sync(this.syncURL, {
        live: true,
        retry: true
      });
    },
    startSync: function() {
      this.syncStatus = "notsyncing";
      if (this.sync) {
        this.sync.cancel();
        this.sync = null;
      }
      if (!this.syncURL) {
        return;
      }
      this.syncStatus = "syncing";
      this.sync = db
        .sync(this.syncURL, {
          live: true,
          retry: true
        })
        .on("change", info => {
          // handle change
          // if this is an incoming change
          if (info.direction == "pull" && info.change && info.change.docs) {
            // loop through all the changes
            for (var i in info.change.docs) {
              var change = info.change.docs[i];
              var arr = null;

              // see if it's an incoming item or list or something else
              if (change._id.match(/^item/)) {
                arr = this.shoppingListItems;
              } else if (change._id.match(/^list/)) {
                arr = this.shoppingLists;
              } else {
                continue;
              }

              // locate the doc in our existing arrays
              var match = this.findDoc(arr, change._id);

              // if we have it already
              if (match.doc) {
                // and it's a deletion
                if (change._deleted == true) {
                  // remove it
                  arr.splice(match.i, 1);
                } else {
                  // modify it
                  delete change._revisions;
                  Vue.set(arr, match.i, change);
                }
              } else {
                // add it
                if (!change._deleted) {
                  arr.unshift(change);
                }
              }
            }
          }
        })
        .on("error", e => {
          this.syncStatus = "syncerror";
        })
        .on("denied", e => {
          this.syncStatus = "syncerror";
        })
        .on("paused", e => {
          if (e) {
            this.syncStatus = "syncerror";
          }
        });
    },
    findDoc: function(docs, id, key) {
      if (!key) {
        key = "_id";
      }
      var doc = null;
      for (var i in docs) {
        if (docs[i][key] == id) {
          doc = docs[i];
          break;
        }
      }
      return { i: i, doc: doc };
    },
    findUpdateDoc: function(docs, id) {
      // locate the doc
      var doc = this.findDoc(docs, id).doc;

      // if it exits
      if (doc) {
        // modift the updated date
        doc.updatedAt = new Date().toISOString();

        // write it on the next tick (to give Vue.js chance to sync state)
        this.$nextTick(() => {
          // write to database
          db.put(doc).then(data => {
            // retain the revision token
            doc._rev = data.rev;
          });
        });
      }
    },
  }
  */
})

import VueI18n from "vue-i18n";
import Vue from "vue";

import en from "./en.json";
import ta from "./tamil.json";
import hd from "./hindi.json"

Vue.use(VueI18n);

const messages = {
  en: en,
  ta: ta,
  hd: hd
};

let locale = localStorage.getItem('locale');

// Create VueI18n instance with options
export default new VueI18n({
  locale: locale || "en", // set locale
  messages: messages // set locale messages,
});

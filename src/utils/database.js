import PouchDB from "pouchdb";
import PouchDBFind from "pouchdb-find";
import VueCookie from "vue-cookie";

PouchDB.plugin(PouchDBFind);
console.log("PouchDBFind",PouchDBFind)
export default {
  jobCardDb:
    VueCookie.get("dbName") != null
      ? new PouchDB(VueCookie.get("dbName"), { skip_setup: true })
      : "",

  getVehicleById(id) {
    return this.jobCardDb.get(id);
  },
  getVehicleInfoByRegistedNumber (registedNumberCode, options) {
    let query = {
      selector: {
        ['vehicleInfo.vehicleNumber']: {$regex: registedNumberCode},
        type: 'jobSheet',
        createdAt: {$gt: null}
      },
      sort: [{createdAt: 'desc'}]
    }
    if (options.selectFields) {
      query.fields = options.selectFields
    }
    return this.jobCardDb.find(query)
  },

  getCVZoneInfo() {
    let query = {
      selector: {
        type: 'cvzoneInfo'
      }
    }
    return this.jobCardDb.find(query)
  },

  startSync(database, keepAlive) {
    if (!database) {
      throw new Error("Missing database");
    }

    //let remoteDBUrl = process.env.remoteDBUrl + database; //http://localhost:5984/
    let remoteDBUrl = "http://localhost:5984/" + database;
    this.initLocalDB(database);

    return this.jobCardDb.sync(remoteDBUrl, {
      live: keepAlive,
      retry: true
    });
    // .on('error', function (err) {
    //   throw err
    // })
  },
  initLocalDB(database) {
    this.jobCardDb = new PouchDB(database, { skip_setup: true });
    this.jobCardDb.createIndex({
      index: {
        fields: ["createdAt"]
      }
    });
  }
};

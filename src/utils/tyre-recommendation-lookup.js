export default {
  TYRE_RECOMMENDATION_LOOKUP: [
    {
      'coach_moderateload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'coach_moderateload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'coach_moderateload_11r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'coach_moderateload_11r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'coach_moderateload_295/80r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduComfort CA',
        'nylonTyre': 'NA'
      }
    },
    {
      'coach_moderateload_295/80r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduComfort CA',
        'nylonTyre': 'NA'
      }
    },
    {
      'coach_ratedload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'coach_ratedload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'coach_ratedload_11r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'coach_ratedload_11r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'coach_ratedload_295/80r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduComfort CA',
        'nylonTyre': 'NA'
      }
    },
    {
      'coach_ratedload_295/80r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduComfort CA',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_heavyload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'LSS XP'
      }
    },
    {
      'longdistance/national_heavyload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_heavyload_10.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_heavyload_11.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_heavyload_11.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_heavyload_11.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_heavyload_12.00-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_heavyload_12.00-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_heavyload_12.00-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_heavyload_12.00-24': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_heavyload_7.00r16': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'Mile Star'
      }
    },
    {
      'longdistance/national_heavyload_7.00r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_heavyload_7.00r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_heavyload_7.50r16': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'LSS XP'
      }
    },
    {
      'longdistance/national_heavyload_7.50r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_heavyload_7.50r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_heavyload_7.50-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_heavyload_7.50-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'longdistance/national_heavyload_7.50-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'longdistance/national_heavyload_8.25r16': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'LSS XP'
      }
    },
    {
      'longdistance/national_heavyload_8.25r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_heavyload_8.25r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_heavyload_8.25r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'LSS GOLD'
      }
    },
    {
      'longdistance/national_heavyload_8.25r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_heavyload_8.25r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_heavyload_9.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_heavyload_9.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_heavyload_9.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_moderateload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'EnduMile LHD',
        'nylonTyre': 'XT7 GOLD HD'
      }
    },
    {
      'longdistance/national_moderateload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_moderateload_10.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'AXVT'
      }
    },
    {
      'longdistance/national_moderateload_11r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_moderateload_11r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_moderateload_11r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RT',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_moderateload_11.00r20': {
        'position': 'Drive',
        'radialTyre': 'Endurace LDR',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_moderateload_11.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace RA1',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_moderateload_11.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace RA1',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_moderateload_12.00-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_moderateload_12.00-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_moderateload_12.00-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_moderateload_12.00-24': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_moderateload_215/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_moderateload_215/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_moderateload_225/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_moderateload_225/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_moderateload_235/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_moderateload_235/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_moderateload_235/75r17.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RT',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_moderateload_295/80r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_moderateload_295/80r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_moderateload_295/80r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_moderateload_7.00r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'Mile Star'
      }
    },
    {
      'longdistance/national_moderateload_7.00r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_moderateload_7.00r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_moderateload_7.50r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'XT7 GOLD+'
      }
    },
    {
      'longdistance/national_moderateload_7.50r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_moderateload_7.50r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_moderateload_7.50-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_moderateload_7.50-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'longdistance/national_moderateload_7.50-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'longdistance/national_moderateload_8.25r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'XT7 GOLD+'
      }
    },
    {
      'longdistance/national_moderateload_8.25r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_moderateload_8.25r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_moderateload_8.25r20': {
        'position': 'Drive',
        'radialTyre': 'EnduRace LD',
        'nylonTyre': 'XT7 GOLD HD'
      }
    },
    {
      'longdistance/national_moderateload_8.25r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_moderateload_8.25r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_moderateload_9.00r20': {
        'position': 'Drive',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_moderateload_9.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_moderateload_9.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_ratedload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'EnduMile LHD',
        'nylonTyre': 'XT-100K'
      }
    },
    {
      'longdistance/national_ratedload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'longdistance/national_ratedload_10.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'AXVT'
      }
    },
    {
      'longdistance/national_ratedload_11r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_ratedload_11r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_ratedload_11r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RT',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_ratedload_11.00r20': {
        'position': 'Drive',
        'radialTyre': 'Endurace LDR',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_ratedload_11.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace RA1',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_ratedload_11.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace RA1',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_ratedload_12.00-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_ratedload_12.00-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_ratedload_12.00-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'longdistance/national_ratedload_12.00-24': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_ratedload_215/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_ratedload_215/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_ratedload_225/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_ratedload_225/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_ratedload_235/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_ratedload_235/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_ratedload_235/75r17.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RT',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_ratedload_295/80r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_ratedload_295/80r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_ratedload_295/80r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'longdistance/national_ratedload_7.00r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'Mile Star'
      }
    },
    {
      'longdistance/national_ratedload_7.00r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_ratedload_7.00r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_ratedload_7.50r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'XT-100K'
      }
    },
    {
      'longdistance/national_ratedload_7.50r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'longdistance/national_ratedload_7.50r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'longdistance/national_ratedload_7.50-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_ratedload_7.50-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'longdistance/national_ratedload_7.50-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'longdistance/national_ratedload_8.25r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'XT-100K'
      }
    },
    {
      'longdistance/national_ratedload_8.25r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'longdistance/national_ratedload_8.25r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'longdistance/national_ratedload_8.25r20': {
        'position': 'Drive',
        'radialTyre': 'EnduRace LD',
        'nylonTyre': 'XT-100K'
      }
    },
    {
      'longdistance/national_ratedload_8.25r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'longdistance/national_ratedload_8.25r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'longdistance/national_ratedload_9.00r20': {
        'position': 'Drive',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'longdistance/national_ratedload_9.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'longdistance/national_ratedload_9.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_heavyload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'EnduTrax MD',
        'nylonTyre': 'LSS XP'
      }
    },
    {
      'mixed(on&offroad)_heavyload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace MA 326',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_heavyload_10.00r20': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace MA 326',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_heavyload_11.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_heavyload_11.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_heavyload_11.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_heavyload_12.00-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_heavyload_12.00-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_heavyload_12.00-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_heavyload_12.00-24': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_heavyload_7.00r16': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'Mile Star'
      }
    },
    {
      'mixed(on&offroad)_heavyload_7.00r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_heavyload_7.00r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_heavyload_7.50r16': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'LSS XP'
      }
    },
    {
      'mixed(on&offroad)_heavyload_7.50r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_heavyload_7.50r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_heavyload_7.50-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_heavyload_7.50-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'mixed(on&offroad)_heavyload_7.50-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'mixed(on&offroad)_heavyload_8.25r16': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'LSS XP'
      }
    },
    {
      'mixed(on&offroad)_heavyload_8.25r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_heavyload_8.25r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_heavyload_8.25r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'LSS GOLD'
      }
    },
    {
      'mixed(on&offroad)_heavyload_8.25r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_heavyload_8.25r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_heavyload_9.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_heavyload_9.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_heavyload_9.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_moderateload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'EnduTrax MD',
        'nylonTyre': 'XT7 GOLD HD'
      }
    },
    {
      'mixed(on&offroad)_moderateload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace MA 326',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_moderateload_10.00r20': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace MA 326',
        'nylonTyre': 'AXVT'
      }
    },
    {
      'mixed(on&offroad)_moderateload_11r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_moderateload_11r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_moderateload_11r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_moderateload_11.00r20': {
        'position': 'Drive',
        'radialTyre': 'Endurace LDR',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_moderateload_11.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace RA1',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_moderateload_11.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace RA1',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_moderateload_12.00-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_moderateload_12.00-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_moderateload_12.00-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_moderateload_12.00-24': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_moderateload_215/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_moderateload_215/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_moderateload_225/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_moderateload_225/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_moderateload_235/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_moderateload_235/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_moderateload_235/75r17.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RT',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_moderateload_295/80r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduTrax MD',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_moderateload_295/80r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_moderateload_295/80r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_moderateload_7.00r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'Mile Star'
      }
    },
    {
      'mixed(on&offroad)_moderateload_7.00r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_moderateload_7.00r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_moderateload_7.50r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'XT7 GOLD+'
      }
    },
    {
      'mixed(on&offroad)_moderateload_7.50r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_moderateload_7.50r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_moderateload_7.50-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_moderateload_7.50-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'mixed(on&offroad)_moderateload_7.50-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'mixed(on&offroad)_moderateload_8.25r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'XT7 GOLD+'
      }
    },
    {
      'mixed(on&offroad)_moderateload_8.25r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_moderateload_8.25r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_moderateload_8.25r20': {
        'position': 'Drive',
        'radialTyre': 'EnduRace LD',
        'nylonTyre': 'XT7 GOLD HD'
      }
    },
    {
      'mixed(on&offroad)_moderateload_8.25r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_moderateload_8.25r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_moderateload_9.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_moderateload_9.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_moderateload_9.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_ratedload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'EnduTrax MD',
        'nylonTyre': 'XT-100K'
      }
    },
    {
      'mixed(on&offroad)_ratedload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace MA 326',
        'nylonTyre': 'XMR'
      }
    },
    {
      'mixed(on&offroad)_ratedload_10.00r20': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace MA 326',
        'nylonTyre': 'AXVT'
      }
    },
    {
      'mixed(on&offroad)_ratedload_11r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_ratedload_11r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_ratedload_11r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_ratedload_11.00r20': {
        'position': 'Drive',
        'radialTyre': 'Endurace LDR',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_ratedload_11.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace RA1',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_ratedload_11.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace RA1',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_ratedload_12.00-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_ratedload_12.00-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_ratedload_12.00-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'mixed(on&offroad)_ratedload_12.00-24': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_ratedload_215/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_ratedload_215/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_ratedload_225/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_ratedload_225/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_ratedload_235/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_ratedload_235/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_ratedload_235/75r17.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RT',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_ratedload_295/80r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduTrax MD',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_ratedload_295/80r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_ratedload_295/80r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'mixed(on&offroad)_ratedload_7.00r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'Mile Star'
      }
    },
    {
      'mixed(on&offroad)_ratedload_7.00r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_ratedload_7.00r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_ratedload_7.50r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'XT-100K'
      }
    },
    {
      'mixed(on&offroad)_ratedload_7.50r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'mixed(on&offroad)_ratedload_7.50r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'mixed(on&offroad)_ratedload_7.50-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_ratedload_7.50-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'mixed(on&offroad)_ratedload_7.50-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'mixed(on&offroad)_ratedload_8.25r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'XT-100K'
      }
    },
    {
      'mixed(on&offroad)_ratedload_8.25r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'mixed(on&offroad)_ratedload_8.25r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'mixed(on&offroad)_ratedload_8.25r20': {
        'position': 'Drive',
        'radialTyre': 'EnduRace LD',
        'nylonTyre': 'XT-100K'
      }
    },
    {
      'mixed(on&offroad)_ratedload_8.25r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'mixed(on&offroad)_ratedload_8.25r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'mixed(on&offroad)_ratedload_9.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'mixed(on&offroad)_ratedload_9.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'mixed(on&offroad)_ratedload_9.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_heavyload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'Endurace RDHD',
        'nylonTyre': 'LSS XP'
      }
    },
    {
      'regional(on-road)_heavyload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_heavyload_10.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_heavyload_11.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_heavyload_11.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_heavyload_11.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_heavyload_12.00-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_heavyload_12.00-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_heavyload_12.00-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_heavyload_12.00-24': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_heavyload_215/75r17.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RT',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_heavyload_7.00r16': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'Mile Star'
      }
    },
    {
      'regional(on-road)_heavyload_7.00r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_heavyload_7.00r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_heavyload_7.50r16': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'LSS XP'
      }
    },
    {
      'regional(on-road)_heavyload_7.50r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_heavyload_7.50r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_heavyload_7.50-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_heavyload_7.50-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'regional(on-road)_heavyload_7.50-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'regional(on-road)_heavyload_8.25r16': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'LSS XP'
      }
    },
    {
      'regional(on-road)_heavyload_8.25r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_heavyload_8.25r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_heavyload_8.25r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'LSS GOLD'
      }
    },
    {
      'regional(on-road)_heavyload_8.25r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_heavyload_8.25r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_heavyload_9.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_heavyload_9.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_heavyload_9.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_moderateload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'EnduRace LD',
        'nylonTyre': 'XT7 GOLD HD'
      }
    },
    {
      'regional(on-road)_moderateload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_moderateload_10.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'AXVT'
      }
    },
    {
      'regional(on-road)_moderateload_11r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_11r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_11r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RT',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_11.00r20': {
        'position': 'Drive',
        'radialTyre': 'Endurace LDR',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_moderateload_11.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace RA1',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_moderateload_11.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace RA1',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_moderateload_12.00-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_moderateload_12.00-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_moderateload_12.00-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_moderateload_12.00-24': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_moderateload_215/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_215/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_215/75r17.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RT',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_225/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_225/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_235/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_235/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_235/75r17.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RT',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_295/80r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_295/80r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_295/80r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_moderateload_7.00r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'Mile Star'
      }
    },
    {
      'regional(on-road)_moderateload_7.00r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_moderateload_7.00r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_moderateload_7.50r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'XT7 GOLD+'
      }
    },
    {
      'regional(on-road)_moderateload_7.50r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_moderateload_7.50r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_moderateload_7.50-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_moderateload_7.50-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'regional(on-road)_moderateload_7.50-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'regional(on-road)_moderateload_8.25r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'XT7 GOLD+'
      }
    },
    {
      'regional(on-road)_moderateload_8.25r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_moderateload_8.25r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_moderateload_8.25r20': {
        'position': 'Drive',
        'radialTyre': 'EnduRace LD',
        'nylonTyre': 'XT7 GOLD HD'
      }
    },
    {
      'regional(on-road)_moderateload_8.25r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_moderateload_8.25r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_moderateload_9.00r20': {
        'position': 'Drive',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_moderateload_9.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_moderateload_9.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_ratedload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'EnduRace LD',
        'nylonTyre': 'XT-100K'
      }
    },
    {
      'regional(on-road)_ratedload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'regional(on-road)_ratedload_10.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'AXVT'
      }
    },
    {
      'regional(on-road)_ratedload_11r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_11r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_11r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RT',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_11.00r20': {
        'position': 'Drive',
        'radialTyre': 'Endurace LDR',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_ratedload_11.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace RA1',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_ratedload_11.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace RA1',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_ratedload_12.00-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_ratedload_12.00-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_ratedload_12.00-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Deluxe'
      }
    },
    {
      'regional(on-road)_ratedload_12.00-24': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_ratedload_215/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_215/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_215/75r17.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RT',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_225/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_225/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_235/75r17.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_235/75r17.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_235/75r17.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RT',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_295/80r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduRace RD',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_295/80r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_295/80r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'NA'
      }
    },
    {
      'regional(on-road)_ratedload_7.00r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'Mile Star'
      }
    },
    {
      'regional(on-road)_ratedload_7.00r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_ratedload_7.00r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_ratedload_7.50r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'XT-100K'
      }
    },
    {
      'regional(on-road)_ratedload_7.50r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'regional(on-road)_ratedload_7.50r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'regional(on-road)_ratedload_7.50-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_ratedload_7.50-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'regional(on-road)_ratedload_7.50-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'Amar'
      }
    },
    {
      'regional(on-road)_ratedload_8.25r16': {
        'position': 'Drive',
        'radialTyre': 'Endurace LD',
        'nylonTyre': 'XT-100K'
      }
    },
    {
      'regional(on-road)_ratedload_8.25r16': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'regional(on-road)_ratedload_8.25r16': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'regional(on-road)_ratedload_8.25r20': {
        'position': 'Drive',
        'radialTyre': 'EnduRace LD',
        'nylonTyre': 'XT-100K'
      }
    },
    {
      'regional(on-road)_ratedload_8.25r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace RA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'regional(on-road)_ratedload_8.25r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'XMR'
      }
    },
    {
      'regional(on-road)_ratedload_9.00r20': {
        'position': 'Drive',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'XT7'
      }
    },
    {
      'regional(on-road)_ratedload_9.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'regional(on-road)_ratedload_9.00r20': {
        'position': 'Trailer',
        'radialTyre': 'Endurace MA',
        'nylonTyre': 'Amar Gold'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_10.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_11.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_11.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_11.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_12.00-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_12.00-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_12.00-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_12.00-24': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_12.00-24': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_12.00-24': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_9.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_9.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_heavyload_9.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'EnduTuff SOD',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace MA 326',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_10.00r20': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace MA 326',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_11r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduTuff SOD',
        'nylonTyre': 'NA'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_11r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_11r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_11.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_11.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_11.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_12.00-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_12.00-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_12.00-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_12.00-24': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_12.00-24': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_12.00-24': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_295/80r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduTrax MD',
        'nylonTyre': 'NA'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_295/80r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_295/80r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_9.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_9.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_moderateload_9.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_10.00r20': {
        'position': 'Drive',
        'radialTyre': 'EnduTuff SOD',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_10.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduRace MA 326',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_10.00r20': {
        'position': 'Trailer',
        'radialTyre': 'EnduRace MA 326',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_11r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduTuff SOD',
        'nylonTyre': 'NA'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_11r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_11r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_11.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_11.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_11.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_12.00-20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_12.00-20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_12.00-20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_12.00-24': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_12.00-24': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_12.00-24': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_295/80r22.5': {
        'position': 'Drive',
        'radialTyre': 'EnduTrax MD',
        'nylonTyre': 'NA'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_295/80r22.5': {
        'position': 'Steer/Dummy',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_295/80r22.5': {
        'position': 'Trailer',
        'radialTyre': 'EnduTrax MA',
        'nylonTyre': 'NA'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_9.00r20': {
        'position': 'Drive',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_9.00r20': {
        'position': 'Steer/Dummy',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    },
    {
      'speciality(mining,sandetc.)_ratedload_9.00r20': {
        'position': 'Trailer',
        'radialTyre': 'NA',
        'nylonTyre': 'MINELUG'
      }
    }

  ]
}

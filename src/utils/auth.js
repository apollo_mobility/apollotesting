import PouchDB from 'pouchdb'
import PouchDBAuthentication from 'pouchdb-authentication'
import db from '@/utils/database'

import VueCookie from 'vue-cookie'

PouchDB.plugin(PouchDBAuthentication)
console.log("PouchDBAuthentication",PouchDBAuthentication)
export default {
  //http://localhost:5984/
 // authDB: new PouchDB(process.env.remoteDBUrl, {skip_setup: true}),
 authDB: new PouchDB("http://localhost:5984/", {skip_setup: true}),
  loggedIn () {
    let authenticated = VueCookie.get('authenticated')
    return authenticated
  },

  async logIn (username, password) {
    try {
      await this.authDB.logIn(username, password)
      let userInfo = await this.authDB.getUser(username)
      await db.startSync(userInfo.metadata.database, false)
      VueCookie.set('dbName', userInfo.metadata.database, { expires: '1Y' })
      VueCookie.set('authenticated', true, { expires: '1Y' })
      return userInfo
    } catch (error) {
      this.authDB.logOut()
      return null
    }
  },

  logOut () {
    VueCookie.delete('authenticated')
    VueCookie.delete('dbName')
    this.authDB.logOut()
  }
}

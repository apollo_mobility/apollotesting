import moment from 'moment'

export default {
  formatDate(date, format = 'DD/MM/YYYY') {
    if(this.isValidateDate(date)) {
      return moment(date).format(format);
    }
    return "";
  },
  addDays(date, days) {
    if(this.isValidateDate(date)) {
      return moment(date).add(Number(days), 'days');
    }
    return "";
  },
  isValidateDate(date) {
    return moment(date).isValid();
  },
  isAfter (endDate, startDate = new Date().toISOString()) {
    if (!this.isValidateDate(endDate) || !this.isValidateDate(startDate)) {
      return false
    }
    return moment(endDate).isAfter(startDate)
  }
}
